import pandas as pd
import matplotlib.pyplot as plt


dfActiv = pd.read_csv("activities.csv", thousands=',')

dfActiv = dfActiv.filter(["Aktivitätstyp", "Datum", "Distanz", "Zeit"])
dfActiv['Distanz'] = dfActiv['Distanz'].astype('float64')
dfActiv['Datum'] = pd.to_datetime(dfActiv['Datum'], yearfirst=True).dt.date
dfActiv.sort_values(by="Datum", ascending=True, inplace=True)

# print(dfActiv["Aktivitätstyp"].unique())

# Einzelne Aktivitäten unterteilen
dfActivGroup = dfActiv.groupby("Aktivitätstyp")
dfRun = dfActivGroup.get_group("Laufen").set_index("Datum")
nRun = dfRun.count()

dfCyc = dfActivGroup.get_group("Radfahren").set_index("Datum")
dfIndCyc = dfActivGroup.get_group("Indoor Cycling").set_index("Datum")
nCyc = dfCyc["Distanz"].count()
nIndCyc = dfIndCyc["Distanz"].count()
nCycTotal = nCyc + nIndCyc

dfSwi = dfActivGroup.get_group("Schwimmen").set_index("Datum")
dfOpenSwi = dfActivGroup.get_group("Freiwasserschwimmen").set_index("Datum")
dfLapSwi = dfActivGroup.get_group("Schwimmbadschwimmen").set_index("Datum")
nSwi = dfSwi["Distanz"].count()
nOpenSwi = dfOpenSwi["Distanz"].count()
nLapSwi = dfLapSwi["Distanz"].count()
nSwiTotal = nSwi + nOpenSwi + nLapSwi

print("Laufen: " + str(nRun.Distanz))
print("Radfahren: " + str(nCycTotal) + " (Draußen: " + str(nCyc) + ", Rolle: " + str(nIndCyc) + ")")
print("Schwimmen: " + str(nSwiTotal) + " (Normal: " + str(nSwi) + ", Freiwasser: " + str(nOpenSwi) + ", Schwimmbad:" + str(nLapSwi) + ")")

start_date = pd.to_datetime('2019-01-01').date()
end_date = pd.to_datetime('2019-12-31').date()
mask = (dfActiv['Datum'] > start_date) & (dfActiv['Datum'] < end_date)
dfRunAlt = dfActiv.loc[mask]
dfRunAlt.set_index("Datum", inplace=True)

dfRunAlt["Distanz"].rolling(10).mean().plot()
plt.ylabel('Distanz')
plt.xlabel('Datum')
# plt.show()
